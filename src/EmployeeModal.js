import React, { Component } from 'react';

class EmployeeModal extends Component {
  constructor(props) {
    super(props)
    this.state = {};
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.deleteEmployee = this.deleteEmployee.bind(this);
    if (this.props.update) {
      this.loadEmployeeData(this.props.info)
    }
  }

  handleInputChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(e) {
    e.preventDefault();
    const data = this.state;
    fetch('http://localhost:8080/api/updateEmployees', {
      method: 'POST',
      mode: 'no-cors',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    });
    this.state = {};
    this.props.toggle();
  }

  handleUpdate(e) {
    e.preventDefault();
    const data = this.state;
    fetch(`http://localhost:8080/api/updateEmployee/${data.id}`, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    });
    this.state = {};
    this.props.toggle();
  }

  deleteEmployee(e) {
    e.preventDefault();
    const data = this.state;
    fetch(`http://localhost:8080/api/deleteEmployee/${data.id}`, {
      method: 'DELETE',
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      }
    });
    this.state = {};
    this.props.toggle();
  }

  loadEmployeeData(info) {
    this.state = {
      id: info.id,
      name: info.name,
      code: info.code,
      profession: info.profession,
      color: info.color,
      city: info.city,
      branch: info.branch,
      assigned: info.assigned,
    }
  }

  render() {
    return (
      <div className="employee-modal">
        <form onSubmit={ this.state.id ? this.handleUpdate : this.handleSubmit }>

         <label>Name</label>
         <input name="name" type="string" value={ this.state.name }  onChange={ this.handleInputChange }/>

         <label>Code</label>
         <input name="code" type="string" value={ this.state.code }  onChange={ this.handleInputChange }/>

         <label>Profession</label>
         <input name="profession" type="string" value={ this.state.profession }  onChange={ this.handleInputChange }/>

         <label>Color</label>
         <input name="color" type="string" value={ this.state.color }  onChange={ this.handleInputChange }/>

         <label>City</label>
         <input name="city" type="string" value={ this.state.city }  onChange={ this.handleInputChange }/>

         <label>Branch</label>
         <input name="branch" type="string" value={ this.state.branch }  onChange={ this.handleInputChange }/>

         <label>Assigned</label>
         <select name="assigned" value={ this.state.assigned } onChange={ this.handleInputChange }>
          <option value="true">true</option>
          <option value="false">false</option>
         </select>

         <input type="submit" value={ this.state.id ? "Update Employee" : "Make Employee" }/>

         {
           this.state.id &&
           <input type="submit" value="Delete Employee" onClick={ this.deleteEmployee }/>
         }
        </form>
      </div>
    )
  }
}

export default EmployeeModal
