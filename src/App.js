import React from 'react';
import Employee from './Employee.js';
import EmployeeModal from './EmployeeModal.js';

class App extends React.Component {
  state = {
    employees: [],
    employeeIds: [],
    showModal: false,
  }

  // componentWillMount = () => {
  //   fetch('http://localhost:8080/api/employees')
  //     .then(response => response.json())
  //     .then(employees => this.setState({ employees }))
  // }

  getEmployees = (e) => {
    this.setState(({ showModal: false }));
    fetch('http://localhost:8080/api/employees')
      .then(response => response.json())
      .then(employees => this.setState({ employees }))
  }

  hideEmployees = (e) => {
    this.setState({ employees: [] });
  }

  toggleEmployeeModal = () => {
    if (this.state.showModal) this.setState({employeeInfo:null})
    this.hideEmployees();
    this.setState({ showModal: !this.state.showModal});
  }

  getEmployeesId = (e) => {
    fetch('http://localhost:8080/api/employees')
      .then(response => response.json())
      .then(employees => {
          this.setState({
            employees: [],
            employeeIds: employees.map(employee => employee['id']),
            showModal: false,
          })
      })
  }

  loadEmployeeInfo = (e) => {
    fetch(`http://localhost:8080/api/employee/${e.target.value}`)
      .then(response => response.json())
      .then(employeeInfo => this.setState({ employeeInfo, employeeIds:[] }))
      .then( () => this.toggleEmployeeModal())
  }


  render() {
    return (
      <div className="App">
        <h1>Plexxis Employees</h1>
        { this.state.employees.length === 0 ? (
          <input type="submit" value="Load Employee List" onClick={ this.getEmployees }/>
        ) : (
          <input type="submit" value="Hide Employee List" onClick={ this.hideEmployees }/>
        ) }
          <input type="submit" value="Make New Employee" onClick={ this.toggleEmployeeModal
          }/>
          <input type="submit" value="Update/Delete Employee Info" onClick={
            this.getEmployeesId
          }/>
          {
            this.state.employeeIds.length > 0 &&
            <div>
            <select defaultValue="nil" onChange={this.loadEmployeeInfo}>
            <option disabled="disabled" value="nil">Choose Employee Id</option>
            {
              this.state.employeeIds.map(id => <option value={ id }>{ id }</option>
              )}
            </select>
            </div>
          }
          {
            this.state.showModal && this.state.employeeInfo &&
              <EmployeeModal toggle={ this.toggleEmployeeModal}  update={ true } info={ this.state.employeeInfo }/>
          }
          {
            this.state.showModal && !this.state.employeeInfo &&
            <EmployeeModal toggle={ this.toggleEmployeeModal } update={ false }/>
          }
        {
          this.state.employees.length > 0 &&
          this.state.employees.map(employee => (
            <Employee key={ employee.id } employee={ employee }></Employee>
          ))
        }
      </div>
    );
  }
}

export default App;
