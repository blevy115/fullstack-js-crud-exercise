import React, { Component } from 'react';

class Employee extends Component {
  constructor(props) {
    super(props)
    this.state = {
      employee: props.employee
    }
  }
  render() {
    const {
      employee
    } = this.state;
    return (
    <div className='info'>
      <ul>{Object.entries(employee).map((info) =>
        <li key={info[0]} className={info[0]}>
          <span className="key">{ info[0] }</span>
          :
          <span className="value">{ info[1].toString() }</span>
        </li>
      )}</ul>
    </div>
  )}
}

export default Employee
