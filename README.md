# Plexxis Interview Exercise

There were a few things I wanted to accomplish while building this CRUD application.  Primarily I wanted something very functional and intuitive.  The first step I took was to create an Employee component separated from app so that I could clean up the app component and display the data as I want to.  Next I worked on the modal that would be used when making changes to the data.  This component was where I spent most of my effort, in conjunction with the index.js file in the server folder as it required the more complex functionality of making changes through api endpoints. Once that was done and working for creating, editing and deleting employees I worked on finishing touches and some basic styling.

There are 2 branches in this repo, the first is master which works as stated with using the json file provided as the REST api, as well as a postgres branch where a potsgres relational database was used which involved making edits to the the server index.js file as the requests now involved using SQL commands to make changes to the database.  However I was unable to have the server work remotely for others so I kept the functional code in a separate branch.

As stated in the instructions first download dependencies with `npm install` and then run `npm start` to start both servers.  Enjoy!
