const express = require('express')
const cors = require('cors')
const app = express()
const employees = require('./data/employees.json');
const bodyParser = require('body-parser')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}))

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}

app.all('/*', (req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
  res.setHeader("Access-Control-Allow-Methods", "*");
  next();
});

app.get('/api/employees', cors(corsOptions), (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees, null, 2));
})

app.get('/api/employee/:id', cors(corsOptions), (req, res, next) => {
  res.setHeader('Content-Type', 'application/json');
  res.status(200);
  res.send(JSON.stringify(employees.filter(employee => employee.id == req.params.id)[0], null, 2));
})

app.post('/api/updateEmployees', (req, res) => {
  let employeeList = employees;
  let newEmployee = Object.keys(req.body)[0];
  let parsedData = JSON.parse(newEmployee);
  parsedData = {
    id: employeeList.length > 0 ? [employeeList.length -1].id + 1 : 1,
    ...parsedData
  }
  employeeList[employeeList.length] = parsedData;
  res.end();
})

app.put('/api/updateEmployee/:id', (req, res) => {
  let employeeList = employees;
  let updateEmployee = Object.keys(req.body)[0];
  let parsedData = JSON.parse(updateEmployee);
  for (let i in employeeList) {
    if (employeeList[i].id == req.params.id){
      employeeList[i] = parsedData;
    }
  }
  res.end();
})

app.delete('/api/deleteEmployee/:id', (req, res) => {
  let employeeList = employees;
  let updateEmployee = Object.keys(req.body)[0];
  let parsedData = JSON.parse(updateEmployee);
  for (let i in employeeList) {
    if (employeeList[i].id == req.params.id) {
      employeeList.splice(i,1);
    }
  }
  res.end();
})

app.listen(8080, () => console.log('Job Dispatch API running on port 8080!'))
